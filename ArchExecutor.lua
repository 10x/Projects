-- Gui to Lua
-- Version: 3.2

-- Instances:

local ScreenGui = Instance.new("ScreenGui")
local Frame = Instance.new("Frame")
local TextBox = Instance.new("TextBox")
local TextButton = Instance.new("TextButton")
local TextLabel = Instance.new("TextLabel")
local TextLabel_2 = Instance.new("TextLabel")
local ImageLabel = Instance.new("ImageLabel")

--Properties:

ScreenGui.Parent = game.Players.LocalPlayer:WaitForChild("PlayerGui")

Frame.Parent = ScreenGui
Frame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
Frame.BorderColor3 = Color3.fromRGB(60, 60, 60)
Frame.Position = UDim2.new(0.22024624, 0, 0.242350057, 0)
Frame.Size = UDim2.new(0, 320, 0, 291)
Frame.Active = true
Frame.Draggable = true
Frame.Visible = true
Frame.Selectable = true

TextBox.Parent = Frame
TextBox.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextBox.BackgroundTransparency = 0.950
TextBox.BorderColor3 = Color3.fromRGB(0, 0, 0)
TextBox.Position = UDim2.new(0.0211537033, 0, 0.284769118, 0)
TextBox.Size = UDim2.new(0, 305, 0, 150)
TextBox.Font = Enum.Font.SourceSans
TextBox.Text = ""
TextBox.TextColor3 = Color3.fromRGB(255, 255, 255)
TextBox.TextSize = 14.000

TextButton.Parent = Frame
TextButton.BackgroundColor3 = Color3.fromRGB(65, 65, 65)
TextButton.BorderColor3 = Color3.fromRGB(0, 0, 0)
TextButton.Position = UDim2.new(0.0242787004, 0, 0.849764645, 0)
TextButton.Size = UDim2.new(0, 304, 0, 29)
TextButton.Font = Enum.Font.SourceSans
TextButton.Text = "Execute, Btw."
TextButton.TextColor3 = Color3.fromRGB(255, 255, 255)
TextButton.TextSize = 14.000

TextLabel.Parent = Frame
TextLabel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.BackgroundTransparency = 1.000
TextLabel.Position = UDim2.new(0.33357057, 0, 0.165104076, 0)
TextLabel.Size = UDim2.new(0, 105, 0, 34)
TextLabel.Font = Enum.Font.Sarpanch
TextLabel.Text = "btw."
TextLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel.TextSize = 27.000

TextLabel_2.Parent = Frame
TextLabel_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_2.BackgroundTransparency = 1.000
TextLabel_2.Position = UDim2.new(0.186695576, 0, 0.0242106114, 0)
TextLabel_2.Size = UDim2.new(0, 216, 0, 41)
TextLabel_2.Font = Enum.Font.Sarpanch
TextLabel_2.Text = "I use arch, Btw."
TextLabel_2.TextColor3 = Color3.fromRGB(255, 255, 255)
TextLabel_2.TextSize = 27.000

ImageLabel.Parent = Frame
ImageLabel.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
ImageLabel.BackgroundTransparency = 1.000
ImageLabel.Position = UDim2.new(0.0213750005, 0, 0.0275429636, 0)
ImageLabel.Size = UDim2.new(0, 67, 0, 64)
ImageLabel.Image = "http://www.roblox.com/asset/?id=10790888490"

TextButton.MouseButton1Down:connect(function()
	loadstring(TextBox.Text)();
end);